from fastapi import FastAPI,Depends,HTTPException,status
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm
import uvicorn
#IIS 架站
#from a2wsgi import ASGIMiddleware
#時間格式
from datetime import datetime,timedelta
from typing import Union, Optional
#資安 JWT
from jose import JWTError, jwt
from passlib.context import CryptContext
# from Model.ItemPOST import Item,User,UserInDB,Token,TokenData
#引入Controller
from Controller import router
#紀錄log
import logging
#讀取ini
import configparser
from pydantic import BaseModel
from typing import Optional
#啟動點
app = FastAPI(title="練習用API", description="練習 FastAPI 並規劃後端模組")

#取得ini資料
config = configparser.ConfigParser()
config.read('Config/TextFile.ini')

#引入Controller
app.include_router(router)
pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")
# OAuth2PasswordBearer 用於檢查 token
oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")

# 密碼驗證函式
def verify_password(plain_password, hashed_password):
    check = False
    
    if plain_password == hashed_password:
        check = True
    #return pwd_context.verify(plain_password, hashed_password)
    return check

# 生成哈希密碼
def get_password_hash(password):
    return pwd_context.hash(password)

# token 驗證函式
def verify_token(token: str = Depends(oauth2_scheme)):
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Invalid token",
        headers={"WWW-Authenticate": "Bearer"},
    )
    try:
        payload = jwt.decode(token, "secret_key", algorithms=["HS256"])
        username: str = payload.get("sub")
        if username is None:
            raise credentials_exception
    except JWTError:
        raise credentials_exception
    if username not in fake_users_db:
        raise credentials_exception
    return username

# 使用 Pydantic 模型定義用戶
class User(BaseModel):
    username: str
    email: Optional[str] = None
    disabled: Optional[bool] = None


# 使用 Pydantic 模型定義用戶
class User(BaseModel):
    username: str
    email: Optional[str] = None
    disabled: Optional[bool] = None

# 使用 Pydantic 模型定義 token
class Token(BaseModel):
    access_token: str
    token_type: str

# 假設的用戶數據庫
fake_users_db = {
    "user": {
        "username": "user",
        "email": "user@example.com",
        #"hashed_password": "$2b$12$KzGQVwA6bUvAH8ToG5X8i.TWtPU6cbK6n9dLhfwgTrkQ6ANVnCu/y", # 密碼是 password
        "hashed_password":"password",
        "disabled": False,
    }
}

# 假設的用戶數據庫
fake_users_db2 = {
    "user": {
        "username": "user",
        "email": "user@example.com",
        "disabled": False,
    }
}

# 假設有一個帳號列表
accounts = [
    {"username": "user1", "email": "user1@example.com"},
    {"username": "user2", "email": "user2@example.com"},
    {"username": "user3", "email": "user3@example.com"}
]

@app.get("/")
def read_main():
    return {"msg": "Hello World"}

@app.get("/API/Accounts")
def get_accounts():
    return accounts

@app.get("/API/School/{student_id}")    
def read_item(student_id: int=1, subject: Optional[str]="Math"):
    """[summary] \n
    :param student_id: 學生編號 \n
    :param subject:科目 \n
    """
    return {"student_id": student_id, "subject": subject}


# 生成 token 的函式
def create_access_token(data: dict, expires_delta: Optional[timedelta] = None):
    to_encode = data.copy()
    if expires_delta:
        expire = datetime.utcnow() + expires_delta
    else:
        expire = datetime.utcnow() + timedelta(minutes=15)
    to_encode.update({"exp": expire})
    encoded_jwt = jwt.encode(to_encode, "secret_key", algorithm="HS256")
    return encoded_jwt

# 使用 OAuth2PasswordBearer 來建立 OAuth2 認證方案
oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")

# 登入端點
@app.post("/token", response_model=Token)
async def login_for_access_token(form_data: OAuth2PasswordRequestForm = Depends()):
    user_dict = fake_users_db.get(form_data.username)
    if not user_dict or not verify_password(form_data.password, user_dict["hashed_password"]):
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect username or password",
            headers={"WWW-Authenticate": "Bearer"},
        )
    access_token_expires = timedelta(minutes=15)
    access_token = create_access_token(
        data={"sub": form_data.username}, expires_delta=access_token_expires
    )
    return {"access_token": access_token, "token_type": "bearer"}

# 獲取當前用戶的函式
async def get_current_user(token: str = Depends(oauth2_scheme)):
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Could not validate credentials",
        headers={"WWW-Authenticate": "Bearer"},
    )
    try:
        payload = jwt.decode(token, "secret_key", algorithms=["HS256"])
        username: str = payload.get("sub")
        if username is None:
            raise credentials_exception
    except JWTError:
        raise credentials_exception
    user = get_user(username)
    if user is None:
        raise credentials_exception
    return user


# 受保護的端點
@app.get("/users/me", response_model=User)
async def read_users_me(current_user: User = Depends(get_current_user)):
    return current_user


# 從數據庫中獲取用戶的函式
def get_user(username: str):
    if username in fake_users_db:
        user_dict = fake_users_db[username]
        return User(**user_dict)

# 受保護的 API 端點
@app.get("/protected")
async def protected_endpoint(username: str = Depends(verify_token)):
    return {"message": f"Welcome, {username}!"}

def common_parameters(
    q: Union[str, None] = None, skip: int = 0, limit: int = 100
):
    return {"q": q, "skip": skip, "limit": limit}


@app.get("/items/")
async def read_items(commons: dict = Depends(common_parameters)):
    return commons


@app.get("/users/")
async def read_users(commons: dict = Depends(common_parameters)):
    return commons


if __name__ == "__main__":
    uvicorn.run(app, host="127.0.0.1",  port=7777)




