from pydantic import BaseModel,Field,validator

#帳號查詢
class SearchList(BaseModel):
    """
    AccID
    Account 帳號
    AccName 帳號名稱
    Email 信箱
    Address 地址
    Paw 密碼 
    IsExists 是否存在 0=不存在,1=存在 
    """
    AccID: str = Field(description="帳號編號")
    Account: str = Field(description="帳號")
    AccName: str = Field(description="帳號名稱")
    Email: str   = Field(description= "信箱")
    Address: str  = Field(description="地址")
    Paw: str   = Field(description="密碼",min_length=1, max_length=16)
    IsExists: int = Field(description="帳號是否存在  0=不存在,1=存在 ")

    model_config = {
        "json_schema_extra": {
            "examples": [
                {
                    "data":[{
                        "AccID" : 1,
                        "Account" : "Mark9999",
                        "AccName" : "Mark",
                        "Email" : "mark111@gmail.com",
                        "Address" : "XX市XX區XX路XX巷XX弄XX號",
                        "Paw" : "qqqee2323",
                        "IsExists" : 1
                    },{
                        "AccID" : 2,
                        "Account" : "Qing888",
                        "AccName" : "Qing",
                        "Email" : "qing111@gmail.com",
                        "Address" : "XX市XX區XX路XX巷XX弄XX號",
                        "Paw" : "qqrrr0883",
                        "IsExists" : 1
                    },]
                    
                }                
            ]
        }
    } 


#新增/編輯
class Account(BaseModel):
    """
    Account 帳號
    AccName 帳號名稱
    Email 信箱
    Address 地址
    Paw 密碼 
    IsExists 是否存在 0=不存在,1=存在 
    """
    Account: str = Field(description="帳號")
    AccName: str = Field(description="帳號名稱")
    Email: str   = Field(description= "信箱")
    Address: str  = Field(description="地址")
    Paw: str   = Field(description="密碼",min_length=1, max_length=16)
    IsExists: int = Field(description="帳號是否存在  0=不存在,1=存在 ")

    model_config = {
        "json_schema_extra": {
            "examples": [
                {
                    "Account" : "Mark9999",
                    "AccName" : "Mark",
                    "Email" : "mark111@gmail.com",
                    "Address" : "XX市XX區XX路XX巷XX弄XX號",
                    "Paw" : "qqqee2323",
                    "IsExists" : 1
                }                
            ]
        }
    } 


class DeletePOST(BaseModel):
    """
    ID 要刪除的ID
    """
    ID: int = Field(description="帳號ID")
    model_config = {
        "json_schema_extra": {
            "examples": [
                {
                    "ID" : 1,
                }                
            ]
        }
    } 
#刪除
# class DeletePOST(BaseModel):
#     """
#     SysID 刪除碼
#     """
#     SysID: int = Field(description="系統編碼")

#     model_config = {
#         "json_schema_extra": {
#             "examples": [
#                 {
#                     "Account" : "Mark9999",
#                     "AccName" : "Mark",
#                     "Email" : "mark111@gmail.com",
#                     "Address" : "XX市XX區XX路XX巷XX弄XX號",
#                     "Paw" : "qqqee2323",
#                     "IsExists" : 1
#                 }                
#             ]
#         }
#     } 