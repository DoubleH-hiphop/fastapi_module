from pydantic import BaseModel,Field,validator

class Token(BaseModel):
    access_token: str
    token_type: str