from pydantic import BaseModel

class Item(BaseModel):
    """
    name 名稱
    description 描述
    price 身價
    """
    name: str
    description: str | None = None
    price: int


class User(BaseModel):
    username: str
    email: str | None = None
    full_name: str |None = None
    disabled: bool | None = None

class UserInDB(User):
    hashed_password: str

class Token(BaseModel):
    access_token: str
    token_type: str


class TokenData(BaseModel):
    username: str | None = None



