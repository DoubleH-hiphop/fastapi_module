from pydantic import BaseModel,Field

class Code(BaseModel):
    """
    Code 狀態代號
    """
    code: int = Field(examples=["777"])