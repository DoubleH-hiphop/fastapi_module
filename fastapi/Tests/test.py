from fastapi.testclient import TestClient
from ...fastapi.app import app

client = TestClient(app)

def test_read_main():
    # 请求 127.0.0.1:8080/
    response = client.get("/")
    assert response.status_code == 200
    assert response.json() == {"msg": "Hello World"}