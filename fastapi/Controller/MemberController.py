from fastapi import APIRouter,Depends
from fastapi.responses import JSONResponse
from Model import MemberSearchListPOST,StatusCode
from mysql.connector import cursor
from Controller.DataBaseController import get_db
from logs import logger
from Controller.DecoraterController import error_handler
from Controller.CodeEnum import Status
#router進入點
router = APIRouter()