from enum import Enum

class Status(str, Enum):
    FailValid = '資料格式不對'
    Search_OK = '查詢成功'
    Search_ERROR = '查詢失敗'
    Insert_OK = '新增成功'
    Insert_ERROR = '新增失敗'
    Update_OK = '更新成功'
    Update_ERROR = '更新失敗'
    Delete_OK = '刪除成功'    
    Delete_ERROR = '刪除失敗'

