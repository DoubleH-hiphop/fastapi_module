import asyncio
from functools import wraps
from fastapi import APIRouter, FastAPI
from fastapi.responses import JSONResponse
from pydantic import BaseModel
from typing import Any, Callable
from logs import logger

router = APIRouter()

class ErrorResponse(BaseModel):
    detail: Any

#同步 驗證資料是否正確
def error_handler_sync(func: Callable) -> Callable:
    #保持資料完整、正確性 避免被串改
    @wraps(func)
    def wrapper(*args, **kwargs):
        try:
            logger.error(e)
            return func(*args, **kwargs)
        except Exception as e:
            logger.error(e)
            return JSONResponse(content=ErrorResponse(detail=str(e), status_code=404))
    return wrapper

#非同步 驗證資料是否正確
def error_handler_async(func: Callable) -> Callable:
    #保持資料完整、正確性 避免被串改
    @wraps(func)
    async def wrapper(*args, **kwargs):
        try:
            logger.error(e)
            return await func(*args, **kwargs)
        except Exception as e:
            logger.error(e)
            return JSONResponse(content=ErrorResponse(detail=str(e), status_code=404))
    return wrapper

#判斷是同步or非同步
def error_handler(func: Callable) -> Callable:
    if asyncio.iscoroutinefunction(func):
        return error_handler_async(func)
    else:
        return error_handler_sync(func)

# @router.get("/v1")
# @error_handler
# async def example_v1():
#     raise ValueError("This is an example error")

# @router.get("/v2")
# @error_handler
# def example_v2():
#     raise ValueError("This is an example error")