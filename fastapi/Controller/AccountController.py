from fastapi import APIRouter,Depends
from fastapi.responses import JSONResponse
from Model import AccountSearchListPOST,StatusCode
from mysql.connector import cursor
from Controller.DataBaseController import get_db
from logs import logger
from Controller.DecoraterController import error_handler
from Controller.CodeEnum import Status
#router進入點
router = APIRouter()

# 假設有一個帳號列表
accounts = [
    {"username": "user1", "email": "user1@example.com"},
    {"username": "user2", "email": "user2@example.com"},
    {"username": "user3", "email": "user3@example.com"}
]

@router.get("/Accounts")
def get_accounts():
    return accounts

#帳號管理 列表查詢
@router.post("/AccountSearchList", 
            summary="帳號管理 列表查詢",
            response_model=AccountSearchListPOST.SearchList,
            responses={
                200: {
                        "description": Status.Search_OK,            
                    },
                404: {"model": StatusCode.Code, "description": Status.Search_ERROR},
                422: { "description": Status.FailValid}
            },)
def SearchList(db:cursor.MySQLCursor = Depends(get_db)) -> dict:
    """
    success: 查詢成功 <br />
    warn: 資料格式不對 <br />
    error: 查詢失敗 <br />
    """
    query = " SELECT * FROM Account WHERE Del=1"
    db.execute(query)
    result = db.fetchall()

    # result = get_db_Accounts()
    if result:    
        return JSONResponse(status_code=200, content={"data": result})
    else:
        logger.error(result)
        return {"error": "User not found"}

#帳號管理 新增
@error_handler
@router.post("/AccountInsert", 
            summary="帳號管理 新增", 
            response_model=Status,
            responses={
                200: {
                        "description": Status.Insert_OK,            
                    },
                404: {"model": StatusCode.Code, "description": Status.Insert_ERROR},
                422: { "description": Status.FailValid}
            },)
def Insert(item:AccountSearchListPOST.Account, db:cursor.MySQLCursor = Depends(get_db)):
    """
    success: 新增成功 <br />
    warn: 資料格式不對 <br />
    error: 新增失敗 <br />
    """
    sql_insert=""" INSERT INTO Account (Account,AccName,AccEmail,AccAddress,Password,Del) 
                   VALUES (%s,%s,%s,%s,%s,%s) """
    db.execute(sql_insert, (item.Account,item.AccName,item.Email,item.Address,item.Paw,item.IsExists))
    result = db.fetchall()
    db.execute("COMMIT")
    logger.info("Insert Success")

    return Status.Insert_OK

#帳號管理 編輯
@error_handler
@router.post("/AccountEdit", 
            summary="帳號管理 編輯",
            response_model=Status,
            responses={
                200: {
                        "description": Status.Update_OK,            
                    },
                404: {"model": StatusCode.Code, "description": Status.Update_ERROR}
            },)
def Edit(AccID:int,item:AccountSearchListPOST.Account, db:cursor.MySQLCursor = Depends(get_db)):
    """
    success: 編輯成功 <br />
    warn: 資料格式不對 <br />
    error: 編輯失敗 <br />
    """
    sql_update=" UPDATE Account SET Account=%s, AccName=%s,AccEmail=%s,AccAddress=%s,Password=%s,Del=%s WHERE AccID=%s "
    db.execute(sql_update, (item.Account,item.AccName,item.Email,item.Address,item.Paw,item.IsExists,AccID))
    result = db.fetchall()
    db.execute("COMMIT")
    logger.info("Update Success")
    return Status.Update_OK

@error_handler
@router.post("/AccountDelete",
             summary="帳號管理 刪除",
             response_model=Status,
             responses={
                 200: {
                     "description": Status.Delete_OK,
                 },
                 404: {
                     "model": StatusCode.Code,
                     "description": Status.Delete_ERROR,
                 },
             })
def Delete(AccID:int, db:cursor.MySQLCursor = Depends(get_db)):
    """
    success: 刪除成功 <br />
    error: 刪除失敗 <br />
    """
    sql_delete="UPDATE Account SET Del=%s WHERE AccID=%s"
    db.execute(sql_delete,(0,AccID))
    result = db.fetchall()
    db.execute("COMMIT")
    logger.info("Delete Success")
    return Status.Delete_OK

#帳號管理 編輯範例
# @error_handler
# @router.post("/AccountEdit_Example", 
#             summary="帳號管理 編輯",
#             response_model=AccountSearchListPOST.Account,
#             responses={
#                 404: {"model": StatusCode.Code, "description": "The item was not found"},
#                 200: {
#                     "description": "Item requested by ID",
#                 },
#                 422: {
#                     "description": "Item requested by ID",
#                 },
#             },)
# def Edit(AccID:int,item:AccountSearchListPOST.Account, db:cursor.MySQLCursor = Depends(get_db)):
#     sql_update=" UPDATE Account SET Account=%s, AccName=%s,AccEmail=%s,AccAddress=%s,Password=%s,Del=%s WHERE SysID=%s "
#     db.execute(sql_update, (item.Account,item.AccName,item.Email,item.Address,item.Paw,item.IsExists,AccID))
#     result = db.fetchall()
#     db.execute("COMMIT")
#     # return {"Update":item.AccName }    
#     return JSONResponse(status_code=404, content={"code": 11223})

#帳號管理 刪除
# @error_handler
# @router.post("/AccountDelete",
#              summary="帳號管理 刪除",
#              response_model=AccountSearchListPOST.Account,
#              response={
#                 404: {"model": StatusCode.Code, "description": "The item was not found"},
#                 200: {
#                     "description": "Item requested by ID",
#                 },
#                 422: {
#                     "description": "Item requested by ID",
#                 },
#             },)

# def Delete(AccID:int,item:AccountSearchListPOST.Account, db:cursor.MySQLCursor = Depends(get_db)):
#     sql_delete = "DELETE FROM Account WHERE SysID=%s "
#     db.execute(sql_delete, (AccID))
#     result = db.fetchall()
#     db.execute("COMMIT")
#     return JSONResponse(status_code=404, content={"code":402})
