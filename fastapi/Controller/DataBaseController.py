from fastapi import Depends
import mysql.connector

def get_db_connection():        
    #建立Connection物件
    conn = mysql.connector.connect(
        #資料庫參數設定
        host="localhost",
        port= 3306,
        user= "root",
        password= "yiqing0420",
        database= "capshop"
    )
    return conn
def get_db():
    conn = get_db_connection() 
    # 建立物件，用於執行SQL查詢
    db = conn.cursor()
    try:
        yield db
    finally:
        db.close()
        conn.close()

