from Controller import AccountController, DecoraterController, LoginController, MemberController, ProductsController, OrdersController
from fastapi import APIRouter

router = APIRouter()

#登入Controller
router.include_router(LoginController.router,tags=["登入"],prefix="/API/Login")
#帳號Controller
router.include_router(AccountController.router,tags=["帳號"],prefix="/API/Account")
#會員Controller
router.include_router(MemberController.router,tags=["會員"],prefix="/API/Member")
#商品Controller
router.include_router(ProductsController.router,tags=["商品"],prefix="/API/Products")
#訂單Controller
router.include_router(OrdersController.router,tags=["訂單"],prefix="/API/Orders")
#錯誤訊息 Controller
router.include_router(DecoraterController.router,tags=["Decorater"],prefix="/API/Decorater")