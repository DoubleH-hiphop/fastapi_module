import os
import time
from loguru import logger

#log檔案路徑
log_path = os.path.join(os.getcwd(), 'logs')

if not os.path.exists(log_path):
    os.mkdir(log_path)

#設定log檔名
log_path_error = os.path.join(log_path, f'{time.strftime("%Y-%m-%d")}_error.log')
#寫入log檔 參數意思: 日誌檔案名稱、每日重寫一個檔案、記錄檔只保留10天(自動刪除)、設定紀錄器非同步寫入紀錄，不會影響到主應用程序的工作
logger.add(log_path_error, rotation="1 day", retention="10 days", enqueue=True)