# FastAPI_Module

### V0.03 FastAPI_Module 更版內容:

##### 帳號管理 加入 刪除 API

##### 調整 log 設定

---

### V0.02 FastAPI_Module 更版內容:

##### 調整 app.py (先改用 app 的方式建 api 而不是 router)

---

### V0.01 FastAPI_Module 更版內容:

##### 建立基本架構，包含 Config(通用參數)、Controller(router)、Model(參數模組)、app.py
