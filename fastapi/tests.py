from fastapi.testclient import TestClient
from app import app
import pytest
client = TestClient(app)

# 假設有一個帳號列表
accounts = [
    {"username": "user1", "email": "user1@example.com"},
    {"username": "user2", "email": "user2@example.com"},
    {"username": "user3", "email": "user3@example.com"}
]

def test_read_main():
    # 请求 127.0.0.1:8080/
    response = client.get("/")
    assert response.status_code == 200
    assert response.json() == {"msg": "Hello World"}

# 使用 TestClient 進行單元測試
def test_get_accounts():
    # 創建測試客戶端
    with TestClient(app) as client:
        # 發送 GET 請求
        response = client.get("/API/Accounts")
        # 確認回應的狀態碼是否為 200 OK
        assert response.status_code == 200
        # 確認回應的內容是否符合預期，這裡假設返回的是 JSON 格式的帳號列表
        assert response.json() == accounts